public class Knight extends Fighter {
    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        if (Utility.Ground == 999) {
            int bound = (int) super.getBaseHp() * 2;
            int x = 0;
            int y = 1;
            int z = x + y;
            while (y <= bound) {
                x = y;
                y = z;
                z = x + y;
            }
            return y;
        }
        if (Utility.isSquare(Utility.Ground)) {
            return super.getBaseHp() * 2;
        }
        if (super.getWp() != 1) {
            return super.getBaseHp() / 10;
        }
        return super.getBaseHp();
    }
}
