public class Warrior extends Fighter {
    public Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        if (Utility.isPrime((int) super.getBaseHp())) {
            return super.getBaseHp() * 2;
        }
        if (super.getWp() != 1) {
            return super.getBaseHp() / 10;
        }
        return super.getBaseHp();
    }
}
